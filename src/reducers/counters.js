export const counterActionTypes = {
    onChangeCounterValue: 'onChangeCounterValue'
};

const initialState = [
    {
        title: 'Counter #1',
        value: 0
    },
    {
        title: 'Counter #2',
        value: 0
    },
    {
        title: 'Counter #3',
        value: 0
    }
];

export default function counters(state = initialState, action) {
    switch (action.type) {
        case 'onChangeCounterValue':
            return state.map(item => {
                if (item.id === action.index) {
                    return {...item, value: action.payload}
                }
                return item
            });
        default:
            return state;
    }
}