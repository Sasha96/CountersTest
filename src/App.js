import React, {Component} from 'react';
import {YellowBox} from 'react-native';
import {Router, Scene, Actions, Tabs, Stack, ActionConst} from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Screens
import Counters from './components/Counters'
import CountersSort from './components/CountersSort'

//redux
import MainReducer from './reducers/reducers';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import logger from 'redux-logger'

export const store = createStore(MainReducer, applyMiddleware(logger));

const TabIcon = ({focused, iconName}) => {
    return <Icon name={iconName} size={30} color={focused?"black":"gray"} />
};

export default class App extends Component {
    constructor() {
        super();
        YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    }
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Scene key="root" hideNavBar>
                        <Scene key="Main" tabs={true} tabBarPosition={"bottom"} showLabel={false} type="reset">
                            <Scene key="CountersTab" iconName="timer" icon={TabIcon}>
                                <Scene key='Counters' title="Counters" component={Counters}/>
                            </Scene>
                            <Scene key="CountersSort" iconName="sort-ascending" icon={TabIcon}>
                                <Scene key='CountersSort' title="CountersSort" component={CountersSort}/>
                            </Scene>
                        </Scene>
                    </Scene>
                </Router>
            </Provider>
        );
    }
}
