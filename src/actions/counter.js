import {store} from '../App';
import {counterActionTypes} from '../reducers/counters';

export const onChangeCounterValue = (index = 0, type = 'Increment') => {
    let counters = store.getState().counters;

    let value;

    if (type === 'Increment') value = counters[index].value++;
    if (type === 'Decrement') value = counters[index].value--;

    store.dispatch({
        type: counterActionTypes.onChangeCounterValue,
        index: index,
        payload: value
    });
};