import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, ScrollView, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import _ from 'lodash';

import * as counterActions from '../actions/counter'

class CounterItem extends Component {
    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps, this.props)
    }

    render() {
        const {counter, index} = this.props;
        return (
            <View style={styles.itemBlock}>
                <Text style={styles.itemTitle}>{counter.title}</Text>
                <View style={styles.itemRightPart}>
                    <TouchableOpacity
                        style={{padding:10}}
                        onPress={()=>counterActions.onChangeCounterValue(index,'Increment')}
                    >
                        <Icon name={'plus-circle'} size={30} color={'black'}/>
                    </TouchableOpacity>
                    <Text style={{fontSize:20, color:'black'}}>{counter.value}</Text>
                    <TouchableOpacity
                        style={{padding:10}}
                        onPress={()=>counterActions.onChangeCounterValue(index,'Decrement')}
                    >
                        <Icon name={'minus-circle'} size={30} color={'black'}/>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }
}

class Counters extends Component {
    render() {
        return (
            <FlatList
                style={styles.container}
                data={_.cloneDeep(this.props.counters)}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item,index}) => <CounterItem counter={item} index={index}/>}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white'
    },
    itemBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1
    },
    itemTitle: {
        fontSize: 20,
        color: 'black'
    },
    itemRightPart: {
        flexDirection: 'row',
        alignItems: 'center'
    }
});

export default connect(
    state => ({
        counters: state.counters
    })
)(Counters);
