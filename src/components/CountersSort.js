import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

class CounterItem extends Component {
    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps, this.props)
    }

    render() {
        const {counter} = this.props;
        return (
            <View style={styles.itemBlock}>
                <Text style={styles.itemTitle}>{counter.title}</Text>
                <View style={styles.itemRightPart}>
                    <Text style={styles.itemValue}>{counter.value}</Text>
                </View>
            </View>
        );
    }
}

class CountersSort extends Component {
    render() {
        return (
            <FlatList
                style={styles.container}
                data={_.cloneDeep(this.props.counters).sort((a,b)=>a.value>b.value)}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => <CounterItem counter={item}/>}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white'
    },
    itemBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1
    },
    itemTitle: {
        fontSize: 20,
        color: 'black'
    },
    itemRightPart: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemValue: {
        fontSize: 20,
        color: 'black',
        padding: 12
    }
});

export default connect(
    state => ({
        counters: state.counters
    })
)(CountersSort);

